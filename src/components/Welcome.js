import React , { Component } from "react"

class Welcome extends Component{
    render(){
        let {name, lastName} = this.props;
        return <h1>Hello {name} {lastName}</h1>
    }
}

export default Welcome; 