import React, { useContext } from "react";
import ComponentAF from "./ComponentAF";
import { UserContext, ChannelContext } from "../App";

function ComponentAE() {
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);
  return (
    <div>
      I am {user} and He is my {channel},
      <ComponentAF />
    </div>
  );
}

export default ComponentAE;
