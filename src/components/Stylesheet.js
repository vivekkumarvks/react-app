import React from "react";
import Inline from "./inline";
import "./myStyles.css";

function Stylesheet(props) {
  let className = props.primary ? "primary" : "";
  return (
    <div>
      <h1 className={`${className} font-xl`}>Stylesheet</h1>
      <Inline />
    </div>
  );
}

export default Stylesheet;
