import React , { Component } from "react"

class Message extends Component{

    constructor(){
        super()
        this.state = {
           message: "Welcome Visitors"
        }
    }

    login(){
        this.setState({
            message: "thank you for subscribing."
        })
    }

    render(){
        let {message} = this.state;
        return (
            <div>
                <h1>{ message }</h1>
                <button onClick={() => this.login()}>Subscribe</button>
            </div>
        )
    }
}

export default Message; 