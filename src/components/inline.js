import React from "react";

function inline() {
  let heading = {
    color: "red",
    fontSize: "100px",
  };
  return (
    <div>
      <div style={heading}>inline</div>
      <h1 className="error">error</h1>
    </div>
  );
}

export default inline;
