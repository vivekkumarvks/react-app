import React from "react";

// function Greet(){
//     return <h1> Hello Vivek</h1>
// }

const Greet = property => {
// console.log(property);
return(
    <div>
        <h1>Hello {property.name} {property.lastName}</h1>
        {property.children}
    </div>
)  
}

export default Greet;