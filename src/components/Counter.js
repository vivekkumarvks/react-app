import React, { Component } from 'react'

class Counter extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         count: 0
      }
    }

    addCount(){
        // this.state.count = this.state.count + 1;
        // this.setState({
        //     count: this.state.count + 1
        // }, ()=> { console.log("Callback Value:-", this.state.count)})
        this.setState((prevState, props) => ({
            count: prevState.count + 1
        }))
        console.log(this.state.count);
    }

    addby2(){
        this.addCount();
        this.addCount();
    }

  render() {
      
    return (
        <div>
            <div>Count: {this.state.count}</div>
            <button onClick={()=> this.addby2()}>Increment</button>
        </div>
    )
  }
}

export default Counter