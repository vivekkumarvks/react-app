import "./App.css";
import React, { Component, Fragment, useReducer } from "react";
import Greet from "./components/Greet";
import Welcome from "./components/Welcome";
import Hello from "./components/Hello";
import Message from "./components/Message";
// import Counter from "./components/Counter";
import FunctionClick from "./components/FunctionClick";
import ClassClick from "./components/ClassClick";
import EventBind from "./components/EventBind";
import ParentComponent from "./components/ParentComponent";
import UserGreeting from "./components/UserGreeting";
import NameList from "./components/NameList";
import Stylesheet from "./components/Stylesheet";
import "./appStyles.css";
import style from "./appStyles.module.css";
import Form from "./components/Form";
import LifecycleA from "./components/LifecycleA";
import FragmentDemo from "./components/FragmentDemo";
import Table from "./components/Table";
import PureComp from "./components/PureComp";
import ParentComp from "./components/ParentComp";
import RefsDemo from "./components/RefsDemo";
import FocusInput from "./components/FocusInput";
import PortalDemo from "./components/PortalDemo";
import Hero from "./components/Hero";
import ErrorBoundary from "./components/ErrorBoundary";
import ClickCounter from "./components/ClickCounter";
import HoverCounter from "./components/HoverCounter";
import Counter from "./components/COunter";
import ClickCounterTwo from "./components/ClickCounterTwo";
import HoverCounterTwo from "./components/HoverCounterTwo";
import ComponentC from "./components/ComponentC";
import PostList from "./components/PostList";
import PostForm from "./components/PostForm";
import ClassCounter from "./components/ClassCounter";
import HookCounter from "./components/HookCounter";
import HookCounterTwo from "./components/HookCounterTwo";
import HookCounterThree from "./components/HookCounterThree";
import HookCounterFour from "./components/HookCounterFour";
import HookCounterOne from "./components/HookCounterOne";
import ClassCounterOne from "./components/ClassCounterOne";
import ClassMouse from "./components/ClassMouse";
import HookMouse from "./components/HookMouse";
import MouseContainer from "./components/MouseContainer";
import IntervalClassCounter from "./components/IntervalClassCounter";
import IntervalHookCounter from "./components/IntervalHookCounter";
import DataFetching from "./components/DataFetching";
import ComponentAC from "./components/ComponentAC";
import CounterOne from "./components/CounterOne";
import CounterTwo from "./components/CounterTwo";
import CounterThree from "./components/CounterThree";
import ComponentAA from "./components/ComponentAA";
import ComponentCA from "./components/ComponentCA";
import ComponentBA from "./components/ComponentBA";
import DataFetchingOne from "./components/DataFetchingOne";
import DataFetchingTwo from "./components/DataFetchingTwo";
import CbParentComponent from "./components/CbParentComponent";
import UseMemoHook from "./components/UseMemoHook";
import UseRefFocusInput from "./components/UseRefFocusInput";
import ClassTimer from "./components/ClassTimer";
import HookTimer from "./components/HookTimer";
import DocTitleOne from "./components/DocTitleOne";
import DocTitleTwo from "./components/DocTitleTwo";
import CustomHookCounterOne from "./components/CustomHookCounterOne";
import CustomHookCounterTwo from "./components/CustomHookCounterTwo";
import UserForm from "./components/UserForm";

const initialState = 0;
const reducer = (state, action) => {
  switch (action) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
    case "reset":
      return initialState;
    default:
      return state;
  }
};

export const CountContext = React.createContext();

export const UserContext = React.createContext();
export const ChannelContext = React.createContext();

function App() {
  const [count, dispatch] = useReducer(reducer, initialState);
  return (
    // <CountContext.Provider
    //   value={{ countState: count, countDispatch: dispatch }}
    // >
    <div className="App">
      <UserForm />
      {/* <CustomHookCounterOne /> */}
      {/* <CustomHookCounterTwo /> */}
      {/* <DocTitleTwo />
      <DocTitleOne /> */}
      {/* <HookTimer />
      <ClassTimer /> */}
      {/* <UseRefFocusInput /> */}
      {/* <UseMemoHook /> */}
      {/* <CbParentComponent /> */}
      {/* <CounterOne /> */}
      {/* <CounterTwo /> */}
      {/* <CounterThree /> */}
      {/* {count}
        <ComponentAA />
        <ComponentBA />
        <ComponentCA /> */}
      {/* <DataFetchingOne /> */}
      {/* <DataFetchingTwo /> */}
    </div>
    // </CountContext.Provider>
  );
}

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <CounterThree />
//         {/* <CounterTwo /> */}
//         {/* <CounterOne /> */}
//         {/* <UserContext.Provider value={"Parent"}>
//           <ChannelContext.Provider value={"Child"}>
//             <ComponentAC />
//           </ChannelContext.Provider>
//         </UserContext.Provider> */}
//         {/* <DataFetching /> */}
//         {/* <IntervalHookCounter />
//         <IntervalClassCounter /> */}
//         {/* <MouseContainer /> */}
//         {/* <HookMouse /> */}
//         {/* <ClassMouse /> */}
//         {/* <HookCounterOne /> */}
//         {/* <ClassCounterOne /> */}
//         {/* <HookCounterFour /> */}
//         {/* <HookCounterThree /> */}
//         {/* <HookCounterTwo /> */}
//         {/* <HookCounter /> */}
//         {/* <ClassCounter /> */}
//         {/* <PostForm /> */}
//         {/* <PostList /> */}
//         {/* <ComponentC /> */}
//         {/* <Counter
//           render={(count, incrementCount) => (
//             <ClickCounterTwo
//               count={count}
//               incrementCount={incrementCount}
//             ></ClickCounterTwo>
//           )}
//         ></Counter>
//         <Counter
//           render={(count, incrementCount) => (
//             <HoverCounterTwo
//               count={count}
//               incrementCount={incrementCount}
//             ></HoverCounterTwo>
//           )}
//         ></Counter> */}
//         {/* <ClickCounter />
//         <HoverCounter /> */}
//         {/* <Hero heroName="Batman" />
//         <Hero heroName="Superman" />
//         <ErrorBoundary>
//           <Hero heroName="Joker" />
//         </ErrorBoundary> */}
//         {/* <PortalDemo /> */}
//         {/* <FocusInput /> */}
//         {/* <RefsDemo /> */}
//         {/* <ParentComp /> */}
//         {/* <PureComp /> */}
//         {/* <Table /> */}
//         {/* <FragmentDemo /> */}
//         {/* <LifecycleA /> */}
//         {/* <Form /> */}
//         {/* <h1 className="error">Error</h1>
//         <h1 className={style.success}>Success</h1>
//         <Stylesheet primary={true} /> */}
//         {/* <NameList/> */}
//         {/* <UserGreeting/> */}
//         {/* <ParentComponent/> */}
//         {/* <EventBind /> */}
//         {/* <FunctionClick />
//       <ClassClick /> */}
//         {/* <Counter /> */}
//         {/* <Message /> */}
//         {/* <Greet name="Vivek" lastName="Srivastava" >
//         <p>My name is Vivek Srivastava.</p>
//       </Greet>
//       <Greet name="Rahul" lastName="Kumar">
//         <button>Click</button>
//       </Greet>
//       <Greet name="Prince" lastName="Verma" />
//       <Welcome name="Vivek" lastName="Srivastava" />
//       <Welcome name="Rahul" lastName="Kumar" />
//       <Welcome name="Prince" lastName="Verma" /> */}
//         {/* <Hello /> */}
//       </div>
//     );
//   }
// }

export default App;
